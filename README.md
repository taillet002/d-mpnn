# README #

Simplified code based on the K. Yang & al. paper, Analyzing Learned Molecular Representations for Property Prediction, J. Chem. Inf. Model. 2019, 59, 8, 3370-3388
https://doi.org/10.1021/acs.jcim.9b00237


Original Code from publication available here
https://github.com/chemprop/chemprop